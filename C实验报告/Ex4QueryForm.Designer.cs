﻿
namespace C实验报告
{
    partial class Ex4QueryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CourseNoTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.CourseNameTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.CourseReditTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.CoursePeriodTextBox = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.QueryBtn = new System.Windows.Forms.Button();
            this.AddBtn = new System.Windows.Forms.Button();
            this.ModifyBtn = new System.Windows.Forms.Button();
            this.DelBtn = new System.Windows.Forms.Button();
            this.outBtn = new System.Windows.Forms.Button();
            this.inBtn = new System.Windows.Forms.Button();
            this.FirstPage = new System.Windows.Forms.Button();
            this.PageUp = new System.Windows.Forms.Button();
            this.PageDown = new System.Windows.Forms.Button();
            this.EndPage = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // CourseNoTextBox
            // 
            this.CourseNoTextBox.Location = new System.Drawing.Point(164, 47);
            this.CourseNoTextBox.Name = "CourseNoTextBox";
            this.CourseNoTextBox.Size = new System.Drawing.Size(148, 31);
            this.CourseNoTextBox.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(66, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 21);
            this.label1.TabIndex = 1;
            this.label1.Text = "课程号：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(360, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 21);
            this.label2.TabIndex = 3;
            this.label2.Text = "课程名：";
            // 
            // CourseNameTextBox
            // 
            this.CourseNameTextBox.Location = new System.Drawing.Point(458, 47);
            this.CourseNameTextBox.Name = "CourseNameTextBox";
            this.CourseNameTextBox.Size = new System.Drawing.Size(148, 31);
            this.CourseNameTextBox.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(672, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 21);
            this.label3.TabIndex = 5;
            this.label3.Text = "学分：";
            // 
            // CourseReditTextBox
            // 
            this.CourseReditTextBox.Location = new System.Drawing.Point(751, 47);
            this.CourseReditTextBox.Name = "CourseReditTextBox";
            this.CourseReditTextBox.Size = new System.Drawing.Size(148, 31);
            this.CourseReditTextBox.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(946, 52);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 21);
            this.label4.TabIndex = 7;
            this.label4.Text = "学时：";
            // 
            // CoursePeriodTextBox
            // 
            this.CoursePeriodTextBox.Location = new System.Drawing.Point(1025, 47);
            this.CoursePeriodTextBox.Name = "CoursePeriodTextBox";
            this.CoursePeriodTextBox.Size = new System.Drawing.Size(148, 31);
            this.CoursePeriodTextBox.TabIndex = 6;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(53, 209);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 72;
            this.dataGridView1.RowTemplate.Height = 33;
            this.dataGridView1.Size = new System.Drawing.Size(1244, 574);
            this.dataGridView1.TabIndex = 8;
            // 
            // QueryBtn
            // 
            this.QueryBtn.Location = new System.Drawing.Point(70, 129);
            this.QueryBtn.Name = "QueryBtn";
            this.QueryBtn.Size = new System.Drawing.Size(148, 51);
            this.QueryBtn.TabIndex = 9;
            this.QueryBtn.Text = "查询";
            this.QueryBtn.UseVisualStyleBackColor = true;
            this.QueryBtn.Click += new System.EventHandler(this.QueryBtn_Click);
            // 
            // AddBtn
            // 
            this.AddBtn.Location = new System.Drawing.Point(279, 129);
            this.AddBtn.Name = "AddBtn";
            this.AddBtn.Size = new System.Drawing.Size(148, 51);
            this.AddBtn.TabIndex = 10;
            this.AddBtn.Text = "添加";
            this.AddBtn.UseVisualStyleBackColor = true;
            this.AddBtn.Click += new System.EventHandler(this.AddBtn_Click);
            // 
            // ModifyBtn
            // 
            this.ModifyBtn.Location = new System.Drawing.Point(499, 129);
            this.ModifyBtn.Name = "ModifyBtn";
            this.ModifyBtn.Size = new System.Drawing.Size(148, 51);
            this.ModifyBtn.TabIndex = 11;
            this.ModifyBtn.Text = ">>去修改>>";
            this.ModifyBtn.UseVisualStyleBackColor = true;
            this.ModifyBtn.Click += new System.EventHandler(this.ModifyBtn_Click);
            // 
            // DelBtn
            // 
            this.DelBtn.Location = new System.Drawing.Point(724, 129);
            this.DelBtn.Name = "DelBtn";
            this.DelBtn.Size = new System.Drawing.Size(148, 51);
            this.DelBtn.TabIndex = 12;
            this.DelBtn.Text = ">>去删除>>";
            this.DelBtn.UseVisualStyleBackColor = true;
            this.DelBtn.Click += new System.EventHandler(this.DelBtn_Click);
            // 
            // outBtn
            // 
            this.outBtn.Location = new System.Drawing.Point(950, 129);
            this.outBtn.Name = "outBtn";
            this.outBtn.Size = new System.Drawing.Size(130, 54);
            this.outBtn.TabIndex = 13;
            this.outBtn.Text = "导出";
            this.outBtn.UseVisualStyleBackColor = true;
            this.outBtn.Click += new System.EventHandler(this.outBtn_Click);
            // 
            // inBtn
            // 
            this.inBtn.Location = new System.Drawing.Point(1144, 129);
            this.inBtn.Name = "inBtn";
            this.inBtn.Size = new System.Drawing.Size(130, 54);
            this.inBtn.TabIndex = 13;
            this.inBtn.Text = "导入";
            this.inBtn.UseVisualStyleBackColor = true;
            this.inBtn.Click += new System.EventHandler(this.inBtn_Click);
            // 
            // FirstPage
            // 
            this.FirstPage.Location = new System.Drawing.Point(466, 832);
            this.FirstPage.Name = "FirstPage";
            this.FirstPage.Size = new System.Drawing.Size(98, 45);
            this.FirstPage.TabIndex = 18;
            this.FirstPage.Text = "首页";
            this.FirstPage.UseVisualStyleBackColor = true;
            this.FirstPage.Click += new System.EventHandler(this.FirstPage_Click);
            // 
            // PageUp
            // 
            this.PageUp.Location = new System.Drawing.Point(584, 832);
            this.PageUp.Name = "PageUp";
            this.PageUp.Size = new System.Drawing.Size(93, 45);
            this.PageUp.TabIndex = 19;
            this.PageUp.Text = "上一页";
            this.PageUp.UseVisualStyleBackColor = true;
            this.PageUp.Click += new System.EventHandler(this.PageUp_Click);
            // 
            // PageDown
            // 
            this.PageDown.Location = new System.Drawing.Point(695, 832);
            this.PageDown.Name = "PageDown";
            this.PageDown.Size = new System.Drawing.Size(92, 45);
            this.PageDown.TabIndex = 19;
            this.PageDown.Text = "下一页";
            this.PageDown.UseVisualStyleBackColor = true;
            this.PageDown.Click += new System.EventHandler(this.PageDown_Click);
            // 
            // EndPage
            // 
            this.EndPage.Location = new System.Drawing.Point(803, 832);
            this.EndPage.Name = "EndPage";
            this.EndPage.Size = new System.Drawing.Size(106, 45);
            this.EndPage.TabIndex = 19;
            this.EndPage.Text = "末页";
            this.EndPage.UseVisualStyleBackColor = true;
            this.EndPage.Click += new System.EventHandler(this.EndPage_Click);
            // 
            // Ex4QueryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1352, 889);
            this.Controls.Add(this.EndPage);
            this.Controls.Add(this.PageDown);
            this.Controls.Add(this.PageUp);
            this.Controls.Add(this.FirstPage);
            this.Controls.Add(this.inBtn);
            this.Controls.Add(this.outBtn);
            this.Controls.Add(this.DelBtn);
            this.Controls.Add(this.ModifyBtn);
            this.Controls.Add(this.AddBtn);
            this.Controls.Add(this.QueryBtn);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.CoursePeriodTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.CourseReditTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.CourseNameTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CourseNoTextBox);
            this.Name = "Ex4QueryForm";
            this.Text = "Ex4Form";
            this.Load += new System.EventHandler(this.Ex4QueryForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox CourseNoTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox CourseNameTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox CourseReditTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox CoursePeriodTextBox;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button QueryBtn;
        private System.Windows.Forms.Button AddBtn;
        private System.Windows.Forms.Button ModifyBtn;
        private System.Windows.Forms.Button DelBtn;
        private System.Windows.Forms.Button outBtn;
        private System.Windows.Forms.Button inBtn;
        private System.Windows.Forms.Button FirstPage;
        private System.Windows.Forms.Button PageUp;
        private System.Windows.Forms.Button PageDown;
        private System.Windows.Forms.Button EndPage;
    }
}