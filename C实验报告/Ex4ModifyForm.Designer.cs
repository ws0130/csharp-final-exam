﻿
namespace C实验报告
{
    partial class Ex4ModifyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ModifyBtn = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.CoursePeriodTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.CourseReditTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.CourseNameTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.CourseNoTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.CourseIdTextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // ModifyBtn
            // 
            this.ModifyBtn.Location = new System.Drawing.Point(342, 360);
            this.ModifyBtn.Name = "ModifyBtn";
            this.ModifyBtn.Size = new System.Drawing.Size(148, 51);
            this.ModifyBtn.TabIndex = 25;
            this.ModifyBtn.Text = "修改课程";
            this.ModifyBtn.UseVisualStyleBackColor = true;
            this.ModifyBtn.Click += new System.EventHandler(this.ModifyBtn_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(237, 292);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 21);
            this.label4.TabIndex = 24;
            this.label4.Text = "学时：";
            // 
            // CoursePeriodTextBox
            // 
            this.CoursePeriodTextBox.Location = new System.Drawing.Point(316, 287);
            this.CoursePeriodTextBox.Name = "CoursePeriodTextBox";
            this.CoursePeriodTextBox.Size = new System.Drawing.Size(227, 31);
            this.CoursePeriodTextBox.TabIndex = 23;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(237, 233);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 21);
            this.label3.TabIndex = 22;
            this.label3.Text = "学分：";
            // 
            // CourseReditTextBox
            // 
            this.CourseReditTextBox.Location = new System.Drawing.Point(316, 228);
            this.CourseReditTextBox.Name = "CourseReditTextBox";
            this.CourseReditTextBox.Size = new System.Drawing.Size(227, 31);
            this.CourseReditTextBox.TabIndex = 21;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(218, 175);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 21);
            this.label2.TabIndex = 20;
            this.label2.Text = "课程名：";
            // 
            // CourseNameTextBox
            // 
            this.CourseNameTextBox.Location = new System.Drawing.Point(316, 170);
            this.CourseNameTextBox.Name = "CourseNameTextBox";
            this.CourseNameTextBox.Size = new System.Drawing.Size(227, 31);
            this.CourseNameTextBox.TabIndex = 19;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(218, 122);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 21);
            this.label1.TabIndex = 18;
            this.label1.Text = "课程号：";
            // 
            // CourseNoTextBox
            // 
            this.CourseNoTextBox.Location = new System.Drawing.Point(316, 117);
            this.CourseNoTextBox.Name = "CourseNoTextBox";
            this.CourseNoTextBox.Size = new System.Drawing.Size(227, 31);
            this.CourseNoTextBox.TabIndex = 17;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(218, 68);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 21);
            this.label5.TabIndex = 27;
            this.label5.Text = "课程Id：";
            // 
            // CourseIdTextBox
            // 
            this.CourseIdTextBox.Location = new System.Drawing.Point(316, 63);
            this.CourseIdTextBox.Name = "CourseIdTextBox";
            this.CourseIdTextBox.Size = new System.Drawing.Size(227, 31);
            this.CourseIdTextBox.TabIndex = 26;
            this.CourseIdTextBox.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // Ex4ModifyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.CourseIdTextBox);
            this.Controls.Add(this.ModifyBtn);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.CoursePeriodTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.CourseReditTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.CourseNameTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CourseNoTextBox);
            this.Name = "Ex4ModifyForm";
            this.Text = "Ex4ModifyForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ModifyBtn;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox CoursePeriodTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox CourseReditTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox CourseNameTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox CourseNoTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox CourseIdTextBox;
    }
}