﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace C实验报告
{
    public partial class Ex4DelForm : Form
    {
        public Ex4DelForm()
        {
            InitializeComponent();
        }

        private void DelBtn_Click(object sender, EventArgs e)
        {
            if (CourseIdTextBox.Text != "")
            {
                if (MessageBox.Show("确定要执行删除课程号为 " + CourseIdTextBox.Text + " 的数据吗？", "提示！", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
                {
                    int cId = Convert.ToInt32(CourseIdTextBox.Text);
                    Course course = new Course(cId);
                    string sql = @"delete from tb_course where cId=@cId";
                    SqlParameter[] paras = { new SqlParameter("@cId",course.CourseId1)};
                    if (DbUtil.ExecuteNonQuery(sql, paras))
                    {
                        MessageBox.Show("删除成功！");
                    }
                    else
                    {
                        MessageBox.Show("删除失败！");
                    }
                }
                else
                {
                    MessageBox.Show("请检查，要删除的数据不能为空。");
                }
            }
        }
    }
}
