﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using System.IO;



namespace C实验报告
{
    public partial class Ex4QueryForm : Form
    {
        Page page = new Page();

        public Ex4QueryForm()
        {
            InitializeComponent();
        }
        /// <summary>
        /// 页面加载时加载
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Ex4QueryForm_Load(object sender, EventArgs e)
        {

            //string cPeriod = CoursePeriodTextBox.Text;
            //string sql = "select * from tb_course where 1=1";
            //List<SqlParameter> paras = new List<SqlParameter>();
            //DataSet ds = DbUtil.ExecuteQuery(sql, paras.ToArray());
            //dataGridView1.DataSource = ds.Tables[0];

            //page.PageSize = 5;
            //page.CurrentPageNo = 1;
            //string sqlForScalar = "select count(1) from tb_course ";
            //string sqlForDataSet = string.Format(@"select * from(select ROW_NUMBER() over(order by cNo) cId,cNo,cName,cRedit,cPeriod from tb_course)as tabel2 where cId between {0} and {1}",
            //    (page.CurrentPageNo - 1) * page.PageSize + 1, page.CurrentPageNo * page.PageSize);


            //PageUtil.GetDataOfCurrentPage(page, sqlForScalar, sqlForDataSet, null);
            //this.dataGridView1.DataSource = page.RecordsOfCurrentPage.Tables[0];
            //MessageBox.Show(page.PageCount.ToString());
            //DataGvStyle();
            ////获取数据行数
            //double DataNum = dataGridView1.Rows.Count;
            //DataCount.Text = "共" + DataNum + "条数据";
        }
        /// <summary>
        /// 首页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FirstPage_Click(object sender, EventArgs e)
        {
            page.CurrentPageNo = 1;
            page.PageSize = 5;
            string sqlForScalar = "select count(1) from tb_course ";
            string sqlForDataSet = string.Format(@"select * from(select ROW_NUMBER() over(order by cNo) cId,cNo,cName,cRedit,cPeriod from tb_course)as tabel2 where cId between {0} and {1}",
                (page.CurrentPageNo - 1) * page.PageSize + 1, page.CurrentPageNo * page.PageSize);


            PageUtil.GetDataOfCurrentPage(page, sqlForScalar, sqlForDataSet, null);
            this.dataGridView1.DataSource = page.RecordsOfCurrentPage.Tables[0];
        }
        /// <summary>
        /// 上一页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PageUp_Click(object sender, EventArgs e)
        {
            page.PageSize = 5;
            if (page.CurrentPageNo == 1)
            {
                MessageBox.Show("当前是第一页");
            }
            else
            {
                page.CurrentPageNo--;
                string sqlForScalar = "select count(1) from tb_course ";
                string sqlForDataSet = string.Format(@"select * from(select ROW_NUMBER() over(order by cNo) cId,cNo,cName,cRedit,cPeriod from tb_course)as tabel2 where cId between {0} and {1}",
                    (page.CurrentPageNo - 1) * page.PageSize + 1, page.CurrentPageNo * page.PageSize);


                PageUtil.GetDataOfCurrentPage(page, sqlForScalar, sqlForDataSet, null);
                this.dataGridView1.DataSource = page.RecordsOfCurrentPage.Tables[0];
                DataGvStyle();

            }
        }
        /// <summary>
        /// 下一页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PageDown_Click(object sender, EventArgs e)
        {
            page.PageSize = 5;
            if (page.CurrentPageNo == page.PageCount)
            {
                MessageBox.Show("当前是末页");
            }
            else
            {
                page.CurrentPageNo++;
                string sqlForScalar = "select count(1) from tb_course ";
                string sqlForDataSet = string.Format(@"select * from(select ROW_NUMBER() over(order by cNo) cId,cNo,cName,cRedit,cPeriod from tb_course)as tabel2 where cId between {0} and {1}",
                    (page.CurrentPageNo - 1) * page.PageSize + 1, page.CurrentPageNo * page.PageSize);


                PageUtil.GetDataOfCurrentPage(page, sqlForScalar, sqlForDataSet, null);
                this.dataGridView1.DataSource = page.RecordsOfCurrentPage.Tables[0];
                DataGvStyle();
            }
        }
        /// <summary>
        /// 末页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EndPage_Click(object sender, EventArgs e)
        {
            page.PageSize = 5;
            page.CurrentPageNo = page.PageCount;
            string sqlForScalar = "select count(1) from tb_course ";
            string sqlForDataSet = string.Format(@"select * from(select ROW_NUMBER() over(order by cNo) cId,cNo,cName,cRedit,cPeriod from tb_course)as tabel2 where cId between {0} and {1}",
                (page.CurrentPageNo - 1) * page.PageSize + 1, page.CurrentPageNo * page.PageSize);


            PageUtil.GetDataOfCurrentPage(page, sqlForScalar, sqlForDataSet, null);
            this.dataGridView1.DataSource = page.RecordsOfCurrentPage.Tables[0];
            DataGvStyle();
        }
        /// <summary>
        /// 清空四个输入框
        /// </summary>
        public void clear()
        {
            CourseNoTextBox.Text = "";
            CourseNameTextBox.Text = "";
            CourseReditTextBox.Text = "";
            CoursePeriodTextBox.Text = "";
        }
        /// <summary>
        /// 设置表样式
        /// </summary>
        public void DataGvStyle()
        {
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView1.Columns[0].HeaderText = "课程ID";
            dataGridView1.Columns[0].ReadOnly = true;
            dataGridView1.Columns[1].HeaderText = "课程号";
            dataGridView1.Columns[2].HeaderText = "课程名";
            dataGridView1.Columns[3].HeaderText = "学分";
            dataGridView1.Columns[4].HeaderText = "学时";
        }
        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddBtn_Click(object sender, EventArgs e)
        {
            string cNo = CourseNoTextBox.Text;
            string cName = CourseNameTextBox.Text;
            string cRedit = CourseReditTextBox.Text;
            string cPeriod = CoursePeriodTextBox.Text;
            if (cNo != "" && cName != "" && cRedit != "" && cPeriod != "")
            {
                Course course = new Course(cNo, cName, Convert.ToDouble(cRedit), Convert.ToInt32(cPeriod));
                string sql = @"insert into tb_course(cNo,cName,cRedit,cPeriod) values(@cNo,@cName,@cRedit,@cPeriod)";
                SqlParameter[] paras = {
                    new SqlParameter("@cNo",course.CourseNo1),
                    new SqlParameter("@cName",course.CourseName1),
                    new SqlParameter("@cRedit",course.CourseRedit1),
                    new SqlParameter("@cPeriod",course.CoursePeriod1)
                };
                if (DbUtil.ExecuteNonQuery(sql, paras))
                {
                    MessageBox.Show("添加成功");
                    clear();
                }
                else
                {
                    MessageBox.Show("添加失败");
                }
            }
            else
            {
                MessageBox.Show("不能插入空数据，请检查！");
            }
        }
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void QueryBtn_Click(object sender, EventArgs e)
        {
            //全部查询
            /*string cNo = CourseNoTextBox.Text;
            string cName = CourseNameTextBox.Text;
            string cRedit = CourseReditTextBox.Text;
            string cPeriod = CoursePeriodTextBox.Text;
            string sql = "select * from tb_course where 1=1";
            List<SqlParameter> paras = new List<SqlParameter>();
            if (cNo != "")
            {
                sql += " and cNo like @Cno";
                paras.Add(new SqlParameter("@cNo", "%" + cNo + "%"));
            }
            if (cName != "")
            {
                sql += " and cName like @cName";
                paras.Add(new SqlParameter("@cName", "%" + cName + "%"));
            }
            if (cRedit != "")
            {
                sql += " and cRedit like @cRedit";
                paras.Add(new SqlParameter("@cRedit", "%" + cRedit + "%"));
            }
            if (cPeriod != "")
            {
                sql += " and cPeriod like @cPeriod";
                paras.Add(new SqlParameter("@cPeriod", "%" + cPeriod + "%"));
            }
            DataSet ds = DbUtil.ExecuteQuery(sql, paras.ToArray());
            dataGridView1.DataSource = ds.Tables[0];
            DataGvStyle();*/
            //分页查询

            page.PageSize = 5;
            page.CurrentPageNo = 1;
            string sqlForScalar = "select count(1) from tb_course ";
            string sqlForDataSet = string.Format(@"select * from(select ROW_NUMBER() over(order by cNo) cId,cNo,cName,cRedit,cPeriod from tb_course)as tabel2 where cId between {0} and {1}",
                (page.CurrentPageNo - 1) * page.PageSize + 1, page.CurrentPageNo * page.PageSize);


            PageUtil.GetDataOfCurrentPage(page, sqlForScalar, sqlForDataSet, null);
            this.dataGridView1.DataSource = page.RecordsOfCurrentPage.Tables[0];
            //MessageBox.Show(page.PageCount.ToString());
            DataGvStyle();

        }
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ModifyBtn_Click(object sender, EventArgs e)
        {
            Ex4ModifyForm mdForm = new Ex4ModifyForm();
            mdForm.ShowDialog();
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DelBtn_Click(object sender, EventArgs e)
        {
            Ex4DelForm delForm = new Ex4DelForm();
            delForm.ShowDialog();
        }
        /// <summary>
        /// 导入数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void inBtn_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = ("Excel文档|*.xlsx");
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                FileStream fs = new FileStream(dialog.FileName, FileMode.Open, FileAccess.ReadWrite);
                XSSFWorkbook book = new XSSFWorkbook(fs);
                //XSSFWorkbook book2 = new XSSFWorkbook(dialog.FileName);
                ISheet sheet = book.GetSheetAt(0);
                //MessageBox.Show(sheet.PhysicalNumberOfRows.ToString());
                List<Course> list = new List<Course>();
                // 表示这是第i行
                for (int i = 1; i < sheet.PhysicalNumberOfRows; i++)
                {
                    IRow row = sheet.GetRow(i);
                    Course c = new Course();
                    // 第j列
                    for (int j = 0; j < row.Cells.Count; j++)
                    {
                        c.CourseId1 = (int)row.GetCell(0).NumericCellValue;
                        c.CourseNo1 = row.GetCell(1).StringCellValue;
                        c.CourseName1 = row.GetCell(2).StringCellValue;
                        c.CourseRedit1 = (double)row.GetCell(3).NumericCellValue;
                        c.CoursePeriod1 = (int)row.GetCell(4).NumericCellValue;
                    }
                    string sql = @"insert into tb_course(cNo,cName,cRedit,cPeriod) values(@cNo,@cName,@cRedit,@cPeriod)";
                    SqlParameter[] paras = {
                    new SqlParameter("@cNo",c.CourseNo1),
                    new SqlParameter("@cName",c.CourseName1),
                    new SqlParameter("@cRedit",c.CourseRedit1),
                    new SqlParameter("@cPeriod",c.CoursePeriod1)
                    };
                    if (DbUtil.ExecuteNonQuery(sql, paras))
                    {
                        MessageBox.Show("第" + i + "行导入成功");
                    }
                    else
                    {
                        MessageBox.Show("导入失败");
                    }
                    list.Add(c);
                }
                dataGridView1.DataSource = list;
                DataGvStyle();
                fs.Close();
                book.Close();
            }
        }
        /// <summary>
        /// 导出数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void outBtn_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "excel file|*.xlsx";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                //新建excel文件的内存模型
                XSSFWorkbook book = new XSSFWorkbook();  //创建工作簿
                ISheet sheet = book.CreateSheet("课程信息表"); //创建工作表
                IRow row = sheet.CreateRow(0);//创建标题行(第一行为标题内容)
                createCell(row, 0, "编号");  //为每列的单元赋值
                createCell(row, 1, "课程号");
                createCell(row, 2, "课程名");
                createCell(row, 3, "学分");
                createCell(row, 4, "学时");

                //MessageBox.Show("have saved,the path is "+dialog.FileName);
                DataTable dt = (DataTable)this.dataGridView1.DataSource; //获取dgvCourse控件中的数据，并转为DataTable
                for (int i = 0; i < dt.Rows.Count; i++)   //循环DataTable的内容
                {
                    row = sheet.CreateRow(i + 1);   //每循环DataTable的一行,相应的Excel表格就需要创建一行
                    for (int j = 0; j < dt.Columns.Count; j++)  //循环列，并为相应的行赋值
                    {
                        //MessageBox.Show(dt.Rows[i][j].ToString());
                        createCell(row, j, dt.Rows[i][j].ToString());
                    }
                }
                FileStream fs = new FileStream(dialog.FileName, FileMode.Create, FileAccess.ReadWrite);
                book.Write(fs);
                fs.Close();
                book.Close();
            }
        }
        /// <summary>
        /// 创建单元格
        /// </summary>
        /// <param name="row">行数</param>
        /// <param name="index">单元格下标</param>
        /// <param name="value">单元格的值</param>
        public void createCell(IRow row, int index, string value)
        {
            ICell cell = row.CreateCell(index);
            cell.SetCellValue(value);
        }

    }
}
