﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace C实验报告
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);


            // 实验一，价格排序
            // 1.创建数组
            double[] price = { 52.80, 35.00, 53.40, 26.25, 26.50, 28.50, 26.80, 28.60, 34.80, 31.50 };
            // 2.排序
            Array.Sort(price);

            Util util = new Util();
            // 3.筛选某个范围内的价格
            util.chose(price, 20, 53);


            // 实验二，编写代码声明学生类和课程类的对象并赋初值
            /*Student student1 = new Student(1, 1, "兰伟师", 22, "男");
            Course course1 = new Course(1, "001", "C#程序设计", 3.0, 16);
            Console.WriteLine(student1.ToString());
            Console.WriteLine(course1.ToString());*/


            //实验三，在实验2的基础上，选用合适的Visual studio的工具箱设计和实现课程的增、删、改、查的用户界面，
            //要求界面美观统一、布局合理，选用控件能恰当地表达业务对象的数据。
            //Application.Run(new Ex3Form());

            //实验四,在实现3的基础上，使用ADO.NET技术完成课程对象信息的增、删、改、查功能（其中查找具有分页功能），
            //其中数据库的操作需具有防SQL注入的功能。安装NPOI插件，并能把查找到的数据导出Excel文件。（编写的代码尽量可复用）
            //Application.Run(new Ex4QueryForm());

        }
        class Util
        {
            public void chose(double[] arr, double min, double max)
            {
                int minIndex = 0;
                int maxIndex = arr.Length - 1;
                // 1.找出最小值下标
                for (int i = 1; i < arr.Length; i++)
                {
                    if (arr[i - 1] <= min && arr[i] > min)
                    {
                        minIndex = i - 1;
                    }
                }
                // 2.找出最大值下标
                for (int i = arr.Length - 2; i >= 0; i--)
                {
                    if (arr[i + 1] >= max && arr[i] < max)
                    {
                        maxIndex = i + 1;
                    }
                }
                // 3.根据两个下标遍历数组，并输出
                for (int i = minIndex; i < maxIndex; i++)
                {
                    Console.WriteLine(arr[i]);
                }
            }
        }
    }
}
