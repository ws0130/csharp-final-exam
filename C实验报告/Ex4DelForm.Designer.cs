﻿
namespace C实验报告
{
    partial class Ex4DelForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label5 = new System.Windows.Forms.Label();
            this.CourseIdTextBox = new System.Windows.Forms.TextBox();
            this.DelBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(218, 102);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 21);
            this.label5.TabIndex = 29;
            this.label5.Text = "课程Id：";
            // 
            // CourseIdTextBox
            // 
            this.CourseIdTextBox.Location = new System.Drawing.Point(316, 97);
            this.CourseIdTextBox.Name = "CourseIdTextBox";
            this.CourseIdTextBox.Size = new System.Drawing.Size(227, 31);
            this.CourseIdTextBox.TabIndex = 28;
            // 
            // DelBtn
            // 
            this.DelBtn.Location = new System.Drawing.Point(316, 270);
            this.DelBtn.Name = "DelBtn";
            this.DelBtn.Size = new System.Drawing.Size(185, 63);
            this.DelBtn.TabIndex = 30;
            this.DelBtn.Text = "删除该课程";
            this.DelBtn.UseVisualStyleBackColor = true;
            this.DelBtn.Click += new System.EventHandler(this.DelBtn_Click);
            // 
            // Ex4DelForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.DelBtn);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.CourseIdTextBox);
            this.Name = "Ex4DelForm";
            this.Text = "Ex4DelForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox CourseIdTextBox;
        private System.Windows.Forms.Button DelBtn;
    }
}