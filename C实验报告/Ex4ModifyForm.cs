﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace C实验报告
{
    public partial class Ex4ModifyForm : Form
    {
        public Ex4ModifyForm()
        {
            InitializeComponent();
        }

        private void ModifyBtn_Click(object sender, EventArgs e)
        {
            int cId = Convert.ToInt32(CourseIdTextBox.Text);
            string cNo = CourseNoTextBox.Text;
            string cName = CourseNameTextBox.Text;
            double cRedit = Convert.ToDouble(CourseReditTextBox.Text);
            int cPeriod = Convert.ToInt32(CoursePeriodTextBox.Text);
            Course course = new Course(cId,cNo, cName, cRedit, cPeriod);
            string sql = @"update tb_course set cNo=@cNo,cName=@cName,cRedit=@cRedit,cPeriod=@cPeriod where cId=@cId";
            SqlParameter[] paras = {
                    new SqlParameter("@cId",course.CourseId1),
                    new SqlParameter("@cNo",course.CourseNo1),
                    new SqlParameter("@cName",course.CourseName1),
                    new SqlParameter("@cRedit",course.CourseRedit1),
                    new SqlParameter("@cPeriod",course.CoursePeriod1)
                };
            if (DbUtil.ExecuteNonQuery(sql, paras))
            {
                MessageBox.Show("修改成功");
            }
            else
            {
                MessageBox.Show("修改失败");
            }

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
