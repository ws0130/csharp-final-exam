﻿
namespace C实验报告
{
    partial class Ex3Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DelBtn = new System.Windows.Forms.Button();
            this.ModifyBtn = new System.Windows.Forms.Button();
            this.AddBtn = new System.Windows.Forms.Button();
            this.QueryBtn = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label4 = new System.Windows.Forms.Label();
            this.CoursePeriodTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.CourseReditTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.CourseNameTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.CourseNoTextBox = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // DelBtn
            // 
            this.DelBtn.Location = new System.Drawing.Point(1029, 146);
            this.DelBtn.Name = "DelBtn";
            this.DelBtn.Size = new System.Drawing.Size(148, 51);
            this.DelBtn.TabIndex = 25;
            this.DelBtn.Text = ">>去删除>>";
            this.DelBtn.UseVisualStyleBackColor = true;
            // 
            // ModifyBtn
            // 
            this.ModifyBtn.Location = new System.Drawing.Point(755, 146);
            this.ModifyBtn.Name = "ModifyBtn";
            this.ModifyBtn.Size = new System.Drawing.Size(148, 51);
            this.ModifyBtn.TabIndex = 24;
            this.ModifyBtn.Text = ">>去修改>>";
            this.ModifyBtn.UseVisualStyleBackColor = true;
            // 
            // AddBtn
            // 
            this.AddBtn.Location = new System.Drawing.Point(462, 146);
            this.AddBtn.Name = "AddBtn";
            this.AddBtn.Size = new System.Drawing.Size(148, 51);
            this.AddBtn.TabIndex = 23;
            this.AddBtn.Text = "添加";
            this.AddBtn.UseVisualStyleBackColor = true;
            // 
            // QueryBtn
            // 
            this.QueryBtn.Location = new System.Drawing.Point(168, 146);
            this.QueryBtn.Name = "QueryBtn";
            this.QueryBtn.Size = new System.Drawing.Size(148, 51);
            this.QueryBtn.TabIndex = 22;
            this.QueryBtn.Text = "查询";
            this.QueryBtn.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(56, 242);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 72;
            this.dataGridView1.RowTemplate.Height = 33;
            this.dataGridView1.Size = new System.Drawing.Size(1244, 645);
            this.dataGridView1.TabIndex = 21;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(950, 69);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 21);
            this.label4.TabIndex = 20;
            this.label4.Text = "学时：";
            // 
            // CoursePeriodTextBox
            // 
            this.CoursePeriodTextBox.Location = new System.Drawing.Point(1029, 64);
            this.CoursePeriodTextBox.Name = "CoursePeriodTextBox";
            this.CoursePeriodTextBox.Size = new System.Drawing.Size(148, 31);
            this.CoursePeriodTextBox.TabIndex = 19;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(676, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 21);
            this.label3.TabIndex = 18;
            this.label3.Text = "学分：";
            // 
            // CourseReditTextBox
            // 
            this.CourseReditTextBox.Location = new System.Drawing.Point(755, 64);
            this.CourseReditTextBox.Name = "CourseReditTextBox";
            this.CourseReditTextBox.Size = new System.Drawing.Size(148, 31);
            this.CourseReditTextBox.TabIndex = 17;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(364, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 21);
            this.label2.TabIndex = 16;
            this.label2.Text = "课程名：";
            // 
            // CourseNameTextBox
            // 
            this.CourseNameTextBox.Location = new System.Drawing.Point(462, 64);
            this.CourseNameTextBox.Name = "CourseNameTextBox";
            this.CourseNameTextBox.Size = new System.Drawing.Size(148, 31);
            this.CourseNameTextBox.TabIndex = 15;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(70, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 21);
            this.label1.TabIndex = 14;
            this.label1.Text = "课程号：";
            // 
            // CourseNoTextBox
            // 
            this.CourseNoTextBox.Location = new System.Drawing.Point(168, 64);
            this.CourseNoTextBox.Name = "CourseNoTextBox";
            this.CourseNoTextBox.Size = new System.Drawing.Size(148, 31);
            this.CourseNoTextBox.TabIndex = 13;
            // 
            // Ex3Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1356, 951);
            this.Controls.Add(this.DelBtn);
            this.Controls.Add(this.ModifyBtn);
            this.Controls.Add(this.AddBtn);
            this.Controls.Add(this.QueryBtn);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.CoursePeriodTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.CourseReditTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.CourseNameTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CourseNoTextBox);
            this.Name = "Ex3Form2";
            this.Text = "Ex3Form2";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button DelBtn;
        private System.Windows.Forms.Button ModifyBtn;
        private System.Windows.Forms.Button AddBtn;
        private System.Windows.Forms.Button QueryBtn;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox CoursePeriodTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox CourseReditTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox CourseNameTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox CourseNoTextBox;
    }
}