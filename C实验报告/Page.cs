﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;

namespace C实验报告
{
    class Page
    {
        public int CurrentPageNo { set; get; }
        public int PageCount { set; get; }
        public int TotalRecords { set; get; }
        public int PageSize { set; get; }
        public DataSet RecordsOfCurrentPage { set; get; }

        public Page()
        {

        }

        public Page(int currentPageNo, int pagecount, int totalrecords, int pagesize, DataSet recordsofcurrentpage)
        {

            CurrentPageNo = currentPageNo;
            PageCount = pagecount;
            TotalRecords = totalrecords;
            PageSize = pagesize;
            RecordsOfCurrentPage = recordsofcurrentpage;
        }
    }
}
