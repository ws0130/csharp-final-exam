﻿
namespace C实验报告
{
    partial class Ex3Form
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.CourseNoTextBox = new System.Windows.Forms.TextBox();
            this.CoursePeriodTextBox = new System.Windows.Forms.TextBox();
            this.CourseReditTextBox = new System.Windows.Forms.TextBox();
            this.CourseNameTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.addButton = new System.Windows.Forms.Button();
            this.DelButton = new System.Windows.Forms.Button();
            this.ModifyButton = new System.Windows.Forms.Button();
            this.QueryButton = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // CourseNoTextBox
            // 
            this.CourseNoTextBox.Location = new System.Drawing.Point(178, 29);
            this.CourseNoTextBox.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.CourseNoTextBox.Name = "CourseNoTextBox";
            this.CourseNoTextBox.Size = new System.Drawing.Size(274, 31);
            this.CourseNoTextBox.TabIndex = 0;
            // 
            // CoursePeriodTextBox
            // 
            this.CoursePeriodTextBox.Location = new System.Drawing.Point(1464, 29);
            this.CoursePeriodTextBox.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.CoursePeriodTextBox.Name = "CoursePeriodTextBox";
            this.CoursePeriodTextBox.Size = new System.Drawing.Size(274, 31);
            this.CoursePeriodTextBox.TabIndex = 2;
            // 
            // CourseReditTextBox
            // 
            this.CourseReditTextBox.Location = new System.Drawing.Point(1031, 29);
            this.CourseReditTextBox.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.CourseReditTextBox.Name = "CourseReditTextBox";
            this.CourseReditTextBox.Size = new System.Drawing.Size(274, 31);
            this.CourseReditTextBox.TabIndex = 3;
            // 
            // CourseNameTextBox
            // 
            this.CourseNameTextBox.Location = new System.Drawing.Point(611, 29);
            this.CourseNameTextBox.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.CourseNameTextBox.Name = "CourseNameTextBox";
            this.CourseNameTextBox.Size = new System.Drawing.Size(274, 31);
            this.CourseNameTextBox.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(72, 37);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 21);
            this.label1.TabIndex = 5;
            this.label1.Text = "课程号：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(505, 37);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 21);
            this.label2.TabIndex = 6;
            this.label2.Text = "课程名：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(925, 37);
            this.label3.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 21);
            this.label3.TabIndex = 7;
            this.label3.Text = "学分：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(1357, 37);
            this.label4.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 21);
            this.label4.TabIndex = 8;
            this.label4.Text = "学时：";
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(178, 129);
            this.addButton.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(277, 79);
            this.addButton.TabIndex = 9;
            this.addButton.Text = "添加";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // DelButton
            // 
            this.DelButton.Location = new System.Drawing.Point(1031, 129);
            this.DelButton.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.DelButton.Name = "DelButton";
            this.DelButton.Size = new System.Drawing.Size(277, 79);
            this.DelButton.TabIndex = 10;
            this.DelButton.Text = "通过课程号进行删除";
            this.DelButton.UseVisualStyleBackColor = true;
            this.DelButton.Click += new System.EventHandler(this.DelButton_Click);
            // 
            // ModifyButton
            // 
            this.ModifyButton.Location = new System.Drawing.Point(1464, 129);
            this.ModifyButton.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.ModifyButton.Name = "ModifyButton";
            this.ModifyButton.Size = new System.Drawing.Size(277, 79);
            this.ModifyButton.TabIndex = 11;
            this.ModifyButton.Text = "通过课程号进行修改";
            this.ModifyButton.UseVisualStyleBackColor = true;
            this.ModifyButton.Click += new System.EventHandler(this.ModifyButton_Click);
            // 
            // QueryButton
            // 
            this.QueryButton.Location = new System.Drawing.Point(611, 129);
            this.QueryButton.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.QueryButton.Name = "QueryButton";
            this.QueryButton.Size = new System.Drawing.Size(277, 79);
            this.QueryButton.TabIndex = 12;
            this.QueryButton.Text = "通过课程名查询";
            this.QueryButton.UseVisualStyleBackColor = true;
            this.QueryButton.Click += new System.EventHandler(this.QueryButton_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(27, 251);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 72;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(1816, 732);
            this.dataGridView1.TabIndex = 13;
            // 
            // Ex3Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1873, 997);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.QueryButton);
            this.Controls.Add(this.ModifyButton);
            this.Controls.Add(this.DelButton);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CourseNameTextBox);
            this.Controls.Add(this.CourseReditTextBox);
            this.Controls.Add(this.CoursePeriodTextBox);
            this.Controls.Add(this.CourseNoTextBox);
            this.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Name = "Ex3Form";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox CourseNoTextBox;
        private System.Windows.Forms.TextBox CoursePeriodTextBox;
        private System.Windows.Forms.TextBox CourseReditTextBox;
        private System.Windows.Forms.TextBox CourseNameTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Button DelButton;
        private System.Windows.Forms.Button ModifyButton;
        private System.Windows.Forms.Button QueryButton;
        private System.Windows.Forms.DataGridView dataGridView1;
    }
}

