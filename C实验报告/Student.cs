﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C实验报告
{
    class Student
    {
        //id，自增
        private int StudentId;
        //班级
        private int StudentClassNo;
        //姓名
        private string StudentName;
        //年龄
        private int StudentAge;
        //性别
        private string StudentSex;

        public Student(int studentId, int studentClassNo, string studentName, int studentAge, string studentSex)
        {
            StudentId = studentId;
            StudentClassNo = studentClassNo;
            StudentName = studentName;
            StudentAge = studentAge;
            StudentSex = studentSex;
        }

        public int StudentId1 { get => StudentId; set => StudentId = value; }
        public int StudentClassNo1 { get => StudentClassNo; set => StudentClassNo = value; }
        public string StudentName1 { get => StudentName; set => StudentName = value; }
        public int StudentAge1 { get => StudentAge; set => StudentAge = value; }
        public string StudentSex1 { get => StudentSex; set => StudentSex = value; }

        public override string ToString()
        {
            return $"{{{nameof(StudentId1)}={StudentId1.ToString()}, " +
                $"{nameof(StudentClassNo1)}={StudentClassNo1.ToString()}, " +
                $"{nameof(StudentName1)}={StudentName1}, " +
                $"{nameof(StudentAge1)}={StudentAge1.ToString()}, " +
                $"{nameof(StudentSex1)}={StudentSex1}}}";
        }
    }
}
