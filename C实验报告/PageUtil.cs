﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;


namespace C实验报告
{
    class PageUtil
    {
        public static void GetDataOfCurrentPage(Page page, string sqlForScalar, string sqlForDataSet, SqlParameter[] paras)
        {

            page.TotalRecords = Convert.ToInt32(DbUtil.ExecuteScalar(sqlForScalar, paras));
            //总页数
            page.PageCount = page.TotalRecords % page.PageSize == 0 ? page.TotalRecords / page.PageSize :
                page.TotalRecords / page.PageSize + 1;
            page.RecordsOfCurrentPage = DbUtil.ExecuteQueryDataSet(sqlForDataSet, paras);


        }
    }
}
