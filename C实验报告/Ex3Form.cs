﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace C实验报告
{
    public partial class Ex3Form : Form
    {
        public Ex3Form()
        {
            InitializeComponent();
        }

        int courseId = 0;
        List<Course> list = new List<Course>();
        /// <summary>
        /// 设置列样式
        /// </summary>
        public void GvStyle()
        {
            dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView1.Columns[0].HeaderText = "课程ID";
            dataGridView1.Columns[1].HeaderText = "课程号";
            dataGridView1.Columns[2].HeaderText = "课程名";
            dataGridView1.Columns[3].HeaderText = "学分";
            dataGridView1.Columns[4].HeaderText = "学时";
        }

        /// <summary>
        /// 清空所有文本框
        /// </summary>
        public void ClearTB()
        {
            CourseNoTextBox.Text = "";
            CourseNameTextBox.Text = "";
            CourseReditTextBox.Text = "";
            CoursePeriodTextBox.Text = "";
        }

        private void addButton_Click(object sender, EventArgs e)
        {

            if (CourseNameTextBox.Text == "" || CourseNoTextBox.Text == "" || CourseReditTextBox.Text == "" || CoursePeriodTextBox.Text == "")
            {
                MessageBox.Show("不能添加空数据,请检查");
            }
            else
            {
                Course corse = new Course(courseId, CourseNoTextBox.Text, CourseNameTextBox.Text, Convert.ToDouble(CourseReditTextBox.Text), Convert.ToInt32(CoursePeriodTextBox.Text));
                courseId++;
                list.Add(corse);
                dataGridView1.DataSource = null;
                dataGridView1.DataSource = list;
                GvStyle();
                MessageBox.Show("添加课程号为 " + CourseNoTextBox.Text + " ,课程名为 " + CourseNameTextBox.Text + " 的数据成功！");
                ClearTB();
            }
        }
        /// <summary>
        /// 通过课程号进行删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DelButton_Click(object sender, EventArgs e)
        {

            if (CourseNoTextBox.Text != "")
            {
                if (MessageBox.Show("确定要执行删除课程号为 " + CourseNoTextBox.Text + " 的数据吗？", "提示！", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
                {
                    foreach (var item in list.ToList())
                    {
                        if (item.CourseNo1 == CourseNoTextBox.Text)
                        {
                            list.Remove(item);
                        }
                    }
                    dataGridView1.DataSource = null;
                    dataGridView1.DataSource = list;

                    GvStyle(); MessageBox.Show("删除课程号为 " + CourseNoTextBox.Text + " 的数据成功！");
                    ClearTB();

                }
                else
                {
                    return;
                }
            }
            else
            {
                MessageBox.Show("请检查，要删除的数据的课程号不能为空。");
            }
        }
        /// <summary>
        /// 通过课程号进行修改
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ModifyButton_Click(object sender, EventArgs e)
        {
            if (CourseNameTextBox.Text != "" && CourseNoTextBox.Text != "" && CourseReditTextBox.Text != "" && CoursePeriodTextBox.Text != "")
            {
                if (MessageBox.Show("确定要执行修改课程号为 " + CourseNoTextBox.Text + " 的数据吗？\n修改后\n课程名: " + CourseNameTextBox.Text + "\n学分: " + CourseReditTextBox.Text + "\n学时: " + CoursePeriodTextBox.Text, "提示！", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
                {
                    foreach (var item in list.ToList())
                    {
                        if (item.CourseNo1 == CourseNoTextBox.Text)
                        {
                            item.CourseName1 = CourseNameTextBox.Text;
                            item.CourseRedit1 = Convert.ToDouble(CourseReditTextBox.Text);
                            item.CoursePeriod1 = Convert.ToInt32(CoursePeriodTextBox.Text);
                        }
                    }
                    dataGridView1.DataSource = null;
                    dataGridView1.DataSource = list;
                    GvStyle();
                    MessageBox.Show("修改成功！");
                    ClearTB();

                }
                else
                {
                    return;
                }
            }
            else
            {
                MessageBox.Show("请检查，要修改的数据不能为空。");
            }
        }
        /// <summary>
        /// 通过课程名进行查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void QueryButton_Click(object sender, EventArgs e)
        {
            if (CourseNameTextBox.Text != "")
            {
                List<Course> newlist = new List<Course>();
                foreach (var item in list.ToList())
                {
                    if (item.CourseName1 == CourseNameTextBox.Text)
                    {
                        newlist.Add(item);
                    }
                }
                if (newlist == null)
                {
                    MessageBox.Show("不存在课程名为" + CourseNameTextBox.Text + "的数据！");
                }
                else
                {
                    dataGridView1.DataSource = null;
                    dataGridView1.DataSource = newlist;
                    GvStyle();
                    MessageBox.Show("查询课程名为" + CourseNameTextBox.Text + "的数据成功！");
                    ClearTB();
                }
                
            }
            else
            {
                MessageBox.Show("查询的课程名不能为空！");

            }


        }



    }
}
