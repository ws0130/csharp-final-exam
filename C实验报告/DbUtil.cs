﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;


namespace C实验报告
{
    class DbUtil
    {
        static string connStr = "server=.;database=Course;user=sa;password=123456";
        /// <summary>
        /// 用于执行非select命令，并返回受影响的行数，大于0则执行成功
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="paras"></param>
        /// <returns></returns>
        public static bool ExecuteNonQuery(string sql,SqlParameter[] paras)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = connStr;
            SqlCommand comm = new SqlCommand();
            comm.Connection = conn;
            comm.CommandText = sql;
            if (paras != null)
            {
                comm.Parameters.AddRange(paras);
            }
            conn.Open();
            int count = comm.ExecuteNonQuery();
            conn.Close();
            return count > 0;
        }
        /// <summary>
        /// 用于执行select语句
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="paras"></param>
        /// <returns></returns>
        public static DataSet ExecuteQuery(string sql, SqlParameter[] paras)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = connStr;
            SqlCommand comm = new SqlCommand();
            comm.Connection = conn;
            comm.CommandText = sql;
            if (paras != null)
            {
                comm.Parameters.AddRange(paras);
            }
            conn.Open();
            SqlDataAdapter sda = new SqlDataAdapter();
            sda.SelectCommand = comm;
            DataSet ds = new DataSet();
            sda.Fill(ds);
            conn.Close();
            return ds;
        }


        public static object ExecuteScalar(string sql, SqlParameter[] paras)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = connStr;
            SqlCommand comm = new SqlCommand();
            comm.Connection = conn;
            comm.CommandText = sql; //为命令对象设置可执行的SQL语句
            if (paras != null)
            {
                comm.Parameters.AddRange(paras); //把参数传递到SQL语句中定义好的参数
            }
            conn.Open();
            Object obj = comm.ExecuteScalar();
            conn.Close();
            return obj;
        }

        public static DataSet ExecuteQueryDataSet(string sql, SqlParameter[] paras)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = connStr;
            SqlCommand comm = new SqlCommand();
            comm.Connection = conn;
            comm.CommandText = sql; //为命令对象设置可执行的SQL语句
            if (paras != null)
            {
                comm.Parameters.AddRange(paras); //把参数传递到SQL语句中定义好的参数
            }
            conn.Open();

            SqlDataAdapter sda = new SqlDataAdapter();
            sda.SelectCommand = comm;

            DataSet ds = new DataSet();
            sda.Fill(ds);
            conn.Close();
            return ds;

        }
    }
}
