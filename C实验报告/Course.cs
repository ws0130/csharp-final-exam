﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C实验报告
{
    class Course
    {
        //id，自增
        private int CourseId;
        //课程号
        private string CourseNo;
        //课程名
        private string CourseName;
        //课程学分
        private double CourseRedit;
        //课程学时
        private int CoursePeriod;

        public Course(int courseId, string courseNo, string courseName, double courseRedit, int coursePeriod)
        {
            CourseId = courseId;
            CourseNo = courseNo;
            CourseName = courseName;
            CourseRedit = courseRedit;
            CoursePeriod = coursePeriod;
        }
        public Course(string courseNo, string courseName, double courseRedit, int coursePeriod)
        {
            CourseNo = courseNo;
            CourseName = courseName;
            CourseRedit = courseRedit;
            CoursePeriod = coursePeriod;
        }
        public Course(int courseId)
        {
            CourseId = courseId;
        }
        public Course()
        {

        }

        public int CourseId1 { get => CourseId; set => CourseId = value; }
        public string CourseNo1 { get => CourseNo; set => CourseNo = value; }
        public string CourseName1 { get => CourseName; set => CourseName = value; }
        public double CourseRedit1 { get => CourseRedit; set => CourseRedit = value; }
        public int CoursePeriod1 { get => CoursePeriod; set => CoursePeriod = value; }

        public override string ToString()
        {
            return $"{{{nameof(CourseId1)}={CourseId1.ToString()}, {nameof(CourseNo1)}={CourseNo1}, {nameof(CourseName1)}={CourseName1}, {nameof(CourseRedit1)}={CourseRedit1.ToString()}, {nameof(CoursePeriod1)}={CoursePeriod1.ToString()}}}";
        }
    }
}
